<?php
namespace Application\Utility;


use Zend\Json\Json;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;

class OrderStatusUtility
{
    protected $userInfo;
    protected $pharmacyId = 0;
    protected $order;
    protected $selfCollect;
    protected $partlyWin;
    protected $buttonType = 0;
    protected $winnerPharmacies;
    protected $winPharm;
    protected $orderModel;
    protected $transactionModel;
    protected $transactionUserModel;
    protected $userModel;
    protected $pushUtility;
    protected $pushTokenList;

    public function __construct($orderModel, $transactionModel, $transactionUserModel, $userModel, $sm)
    {
        $this->orderModel = $orderModel;
        $this->transactionModel = $transactionModel;
        $this->transactionUserModel = $transactionUserModel;
        $this->userModel = $userModel;
        $this->pushUtility = new PushUtility($sm, 'en');
    }

//   ... continue ...

    public function getPartlyWin()
    {
        return $this->partlyWin;
    }

    public function setButtonType($buttonType)
    {
        $this->buttonType = $buttonType;

        return $this;
    }

//    ... continue ...

    public function setStatusForCustomer()
    {
        $transaction = $this->transactionModel->findOneBy(['order' => $this->order['order_id']]);
        $messages = null;

        if ($this->getSelfCollect() == 1) {
            if ($this->getPartlyWin() == 1) {
                if ($this->winPharm) {
                    $cPharmacyPrepare = sizeof(array_keys(array_column($this->getWinnerPharmacies(), 'status_id'), 4));
                    $cPharmacyAssembled = sizeof(array_keys(array_column($this->getWinnerPharmacies(), 'status_id'), 12));
                    $cNotReceived = sizeof(array_keys(array_column($this->getWinnerPharmacies(), 'status_id'), 8));
                    $cWaite = $cPharmacyPrepare + $cPharmacyAssembled;

                    if ($this->winPharm['status_id'] == 12 && ($this->order['status_id'] == 4 || $this->order['status_id'] == 12)) {
                        $this->setPushTokenList($this->winPharm['pharmacy_id']);
                        if ($this->getButtonType() == 1) {
                            /**** тап по кнопке "получил" ****/
                            $this->orderModel->updateOrderStatusPharmacy($this->order['order_id'], $this->winPharm['pharmacy_id'], 7);
                            if ($cWaite == 1 && $cNotReceived == 0) {
                                $this->orderModel->changeOrderStatusById($this->order['order_id'], 7);
                                $this->transactionUserModel->changeStatus($transaction->getId(), 7);
                            } elseif($cWaite == 1 && $cNotReceived > 0) {
                                $this->orderModel->changeOrderStatusById($this->order['order_id'], 8);
                                $this->transactionUserModel->changeStatus($transaction->getId(), 8);
                            }

                            $this->pushUtility
                                ->setPushTokenList($this->pushTokenList)
                                ->setPushType(3)
                                ->setElementId($this->order['order_id'])
                                ->setStatusId(7)
                                ->send();
                        } else {
                            $messages[] = 'Button type is not valid';
                        }
                    } else {
                        $messages[] = 'The status can not be changed';
                    }
                } else {
                    $messages[] = 'Pharmacy not found';
                }
            } else {
                if ($this->winPharm['status_id'] == 12 && $this->order['status_id'] == 12) {
                    $this->setPushTokenList($this->winPharm['pharmacy_id']);

//                    ... continue ...


                } else {
                    $messages[] = 'The status can not be changed';
                }
            }
        } else {
            /*** доставка ***/
            if ($this->getPartlyWin() == 1) {
                if ($this->order['status_id'] == 5) {
                    $winPharmKey = array_keys(array_column($this->getWinnerPharmacies(), 'winner'), 1);
                    $winPharm = $this->getWinnerPharmacies()[$winPharmKey[0]];
                    $this->setPushTokenList($winPharm['pharmacy_id']);

                    if ($this->getButtonType() == 1) {
                        /**** тап по кнопке "Заказ доставлен" ****/
                        $this->orderModel->updateOrderStatusPharmacyByOrderId($this->order['order_id'], 7);
                        $this->orderModel->changeOrderStatusById($this->order['order_id'], 7);
                        $this->transactionUserModel->changeStatus($transaction->getId(), 7);

                        $this->pushUtility
                            ->setPushTokenList($this->pushTokenList)
                            ->setPushType(3)
                            ->setElementId($this->order['order_id'])
                            ->setStatusId(7)
                            ->send();
                    } elseif($this->getButtonType() == 2) {
                        /**** тап по кнопке "Заказ не доставлен" ****/

//                        ... continue ...

                        } else {
                            $messages[] = 'Pharmacy not found';
                        }
                    } else {
                        $messages[] = 'Button type is not valid';
                    }
                }
            }
//        ... continue ...
        return $messages;
    }
}