<?php
/**
 * Copyright (c) 2017, VironIT and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * @author   VironIT
 * @version  2.9
 * @since    1.0
 */
namespace Admin\Controller;

use Admin\Form\DoctorForm;
use Zend\View\Model\ViewModel;

/**
 * Class DoctorController
 *
 * @package Admin\Controller
 */
class DoctorController extends DefaultController
{
    /**
     * @var DoctorModel
     */
    protected $doctorModel;

    /**
     * @param $sm
     */
    public function setSm($sm)
    {
        $this->doctorModel = $this->sm->get('DoctorModel');
    }

    /**
     * Default action
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        $doctorList = $this->doctorModel->fetchAll();

        return new ViewModel(
            [
                'requestUri' => $this->getRequest()->getRequestUri(),
                'doctorList'    => $doctorList,
            ]
        );
    }

    /**
     * Adding a new doctor
     *
     * @return \Zend\Http\Response|ViewModel
     */
    public function createAction()
    {
        $request = $this->getRequest();
        $postData = $request->getPost();
        $requestUri = $this->replaceUri('create');

        $form = new DoctorForm('DoctorForm');
        if ($request->isPost()) {
            $form->setData($postData);

            if ($form->isValid()) {
                $formData = $form->getData();

                $doctor = $this->doctorModel->findOneBy(['name' => $formData['name']]);
                if ($doctor) {
                    $this->flashMessenger()->addMessage([[
                        'error' => 'Doctor with this name already exists'
                    ]]);
                } else {
                    $newDoctor = $this->doctorModel->create($formData);
                    if ($newDoctor) {
                        $this->flashMessenger()->addMessage([[
                            'success' => 'Doctor successfully added'
                        ]]);

                        return $this->redirect()->toUrl($requestUri);
                    } else {
                        $this->flashMessenger()->addMessage([[
                            'error' => 'Sorry, the doctor cannot be created, please try again'
                        ]]);
                    }
                }
            } else {
                $messages = null;
                foreach ($form->getMessages() as $getMessages) {
                    foreach ($getMessages as $msg) {
                        $messages[] = [
                            'error' => $msg,
                        ];
                    }
                }
                $this->flashMessenger()->addMessage($messages);
            }
        }

        return new ViewModel(
            [
                'form'       => $form,
                'requestUri' => $requestUri,
            ]
        );
    }

    /**
     * Editing information about the doctor
     *
     * @return \Zend\Http\Response|ViewModel
     */
    public function editAction()
    {
        $request = $this->getRequest();
        $postData = $request->getPost();
        $requestUri = $this->replaceUri('edit');
        $docId = $this->params()->fromRoute('id');

        $form = new DoctorForm('DoctorForm');
        if ($request->isPost()) {
            $form->setData($postData);

            if ($form->isValid()) {
                $formData = $form->getData();

                $doctor = $this->doctorModel->findOneBy(['name' => $formData['name']]);
                if ($doctor && $doctor->getId() != $docId) {
                    $this->flashMessenger()->addMessage([[
                        'error' => 'Doctor with this name already exists'
                    ]]);
                } else {
                    $editingDoctor = $this->doctorModel->updateRecord($docId, $formData);
                    if ($editingDoctor) {
                        $this->flashMessenger()->addMessage([[
                            'success' => 'Doctor successfully edited'
                        ]]);

                        return $this->redirect()->toUrl($requestUri);
                    }
                }
            } else {
                $messages = null;
                foreach ($form->getMessages() as $getMessages) {
                    foreach ($getMessages as $msg) {
                        $messages[] = [
                            'error' => $msg,
                        ];
                    }
                }
                $this->flashMessenger()->addMessage($messages);
            }
        } else {
            $doctor = $this->doctorModel->findOneBy(['id' => $docId]);
            if ($doctor) {
                $data['name'] = $doctor->getName();
                $form->setData($data);
            } else {
                $this->flashMessenger()->addMessage([[
                    'error' => 'Sorry, doctor does not exist'
                ]]);
                return $this->redirect()->toUrl($requestUri);
            }
        }
        return new ViewModel(
            [
                'form'       => $form,
                'requestUri' => $requestUri,
            ]
        );
    }

    /**
     * Removal of the doctor
     *
     * @return \Zend\Http\Response
     */
    public function deleteAction()
    {
        $data = $this->params()->fromPost();
        $docId = (int)$data['docid'];
        if (isset($docId) && !empty($docId)) {
            $doctor = $this->doctorModel->findOneBy(['id' => $docId]);
            if ($doctor) {
                $this->doctorModel->deleteRecord($doctor);
                $this->flashMessenger()->addMessage([[
                    'success' => 'Doctor successfully deleted'
                ]]);
            }
        }
        if (!$this->flashMessenger()->hasMessages()) {
            $this->flashMessenger()->addMessage([[
                'error' => 'Sorry, the doctor cannot be deleted, please try again'
            ]]);
        }
        return $this->redirect()->toUrl($this->replaceUri('delete'));
    }
}